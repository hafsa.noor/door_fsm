Door is being controlled by fsm. 
FSM included in the project is tinyfsm git module available at  https://github.com/digint/tinyfsm.git.
The machine has 3 States 1.Locked (Intialized at this state)
                         2.Closed(Unlocked)
                         3.Open
The machine has 4 events that are used to transition between the states.
                         1.Locking     
                         2.Unlocking
                         3.Close
                         4.Open

                                                                 

Door.hpp file    ----> Contains the class of main FSM
States.hpp file  ----> Contains classes of events derived from the main FSM Class included in Door.hpp 
                       Overrides function for each event.
dispatch.hpp     ----> The send_event function in dispatch.hpp uses the dispatch function        included in            tinyfsm.hpp to call out events.  