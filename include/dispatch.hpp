#ifndef DISPATCH_HPP_INCLUDED
#define DISPATCH_HPP_INCLUDED

#include<ctime>
#include "Door.hpp"
#include "States.hpp"
#include <tinyfsm.hpp>

using fsm_list = tinyfsm::FsmList<Door>;

template <typename E>
void send_event(E const &event)
{  // cout<<"SEND EVENT"<<endl;
   fsm_list::template dispatch<E>(event);
}

#endif