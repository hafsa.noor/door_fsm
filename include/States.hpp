#ifndef STATES_HPP
#define STATES_HPP

#include "Door.hpp"

#include <iostream>

using namespace std;

// 3 States  State_Open SClock State_Locked
// 4 EVENTS Open Close Lock Unlock
// D_State=1 OPEN : D_State=0 CLOSE
// L_state=0 UNLocked : L_state=1 LOCKED
enum
{
   Closed = 0,
   Opened
};
enum
{
   Unlocked = 0,
   Locked
};

class State_Closed;  // Forward declaration
class State_Locked;

bool Door::check_time()
{
   time_t ttime      = time(0);
   tm *   local_time = localtime(&ttime);
   int    hour       = local_time->tm_hour;

   if (hour >= 9 && hour <= 18)
      return true;
   else
      return false;
}

void Door::current_state()
// D_State=1 Opened : D_State=0 Closed
// L_state=0 Unlocked : L_state=1 Locked

{
   if (check_time() == false)
   {
      cout << "Door remains Locked between 9am and 6pm- Can not Unlock." << endl;
      return;
   }
   if (D_state == Opened)
   {
      cout << "Door is OPEN" << endl;
   }
   if (D_state == Closed && L_state == Unlocked)
   {
      cout << "Door is CLOSED and UN-LOCKED" << endl;
   }

   if (D_state == Closed && L_state == Locked)
   {
      cout << "Door is CLOSED and LOCKED" << endl;
   }
}

class State_Open : public Door
{  // D_State=1 OPEN : D_State=0 CLOSE
   // L_state=0 UNLocked : L_state=1 LOCKED

   void entry() override
   {
      cout << "Entered Open State" << endl;
      current_state();
   }

   void react(Open const &) override
   {
      cout << "Door is already Open" << endl;
      return;
   }

   // friend class Door;
   void react(Close const &) override
   {
      D_state = Closed;
      // cout<<"TRANSIT to Closed"<<endl;
      transit<State_Closed>();
   }

   void react(Locking const &) override
   {
      cout << "Can not lock. Door is Open. Close the door first. " << endl;
      return;
   }
   void react(Unlocking const &) override
   {
      cout << "Can not unlock. Door is Open. Close the door first.  " << endl;
      return;
   }

   void exit() override
   {
      cout << "EXIT  Open State" << endl;
   }
};

// State Closed
class State_Closed : public Door
{
   // friend class Door;
   void entry() override
   {
      cout << "Entered Closed State" << endl;
      current_state();
   }
   void react(Open const &) override
   {
      if (D_state == Closed && L_state == Unlocked)
      {  // Door is closed and unlocked
         D_state = Opened;
         // cout<<"TRANSIT to Open"<<endl;    // Change door state at transit
         transit<State_Open>();
      }
   }
   void react(Close const &) override
   {
      cout << "Door is already Closed " << endl;
   }

   void react(Locking const &) override
   {
      if (D_state == Closed && L_state == Unlocked)
      {  // currently unlocked
         cout << "if 1" << endl;
         L_state = Locked;
         // cout<<"TRANSIT to Locked"<<endl;    // switched to locked
         transit<State_Locked>();
      }
   }
   void react(Unlocking const &) override
   {
      if (D_state == Closed && L_state == Unlocked)  // MARKER
      {
         cout << "Door is already Un-locked" << endl;
      }
   }
   void exit() override
   {
      cout << "EXIT Closed State" << endl;  //  current_state();
   }
};

class State_Locked : public Door
{  // D_State=1 Opened : D_State=0 Closed
   // L_state=0 Unlocked : L_state=1 Locked

   void entry() override
   {
      cout << "Entered Locked State" << endl;
      current_state();
   }

   void react(Open const &) override
   {
      cout << "Can not Open. Door is Locked. Un-lock the door first." << endl;
      // current_state();
      return;
   }
   void react(Close const &) override
   {
      cout << "Door is already Closed" << endl;
      // current_state();
      // transit<State_Closed>();
      return;
   }
   void react(Locking const &) override
   {
      cout << "Slocked Locking react" << endl;

      if (D_state == Closed && L_state == Unlocked)
      {  // Door is Closed and Unlocked
         L_state = Locked;
         // cout<<"TRANSIT to Locked"<<endl;    // Locked
         transit<State_Locked>();
      }

      else if (L_state == Locked)

      {  // Door is Closed and locked
         cout << "Door is already Locked" << endl;
      }
   }

   void react(Unlocking const &) override
   {
      // D_State=1 Opened : D_State=0 Closed
      // L_state=0 Unlocked : L_state=1 Locked
      bool flag = check_time();
      if (flag == false)
      {
         current_state();
      }
      else if (D_state == Closed && L_state == Locked)

      {  // Door closed and Locked
         // cout<<"TRANSIT to Closed"<<endl;
         L_state = Unlocked;  // Unlocked
         transit<State_Closed>();
      }
   }

   void exit() override
   {

      cout << "EXIT Locked State" << endl;

      // current_state();
   }
};

FSM_INITIAL_STATE(Door, State_Locked)

#endif